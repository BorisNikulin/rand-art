# Random Art
[![pipeline status](https://gitlab.com/BorisNikulin/rand-art/badges/master/pipeline.svg)](https://gitlab.com/BorisNikulin/rand-art/commits/master)
[![coverage report](https://gitlab.com/BorisNikulin/rand-art/badges/master/coverage.svg)](https://borisnikulin.gitlab.io/rand-art/master/coverage)

Random art through the use of Haskell
