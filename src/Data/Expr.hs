module Data.Expr
	( module System.Random
	, Expr(..)
	, eval
	, buildRandomExpr
	) where

import Control.Monad.State
import System.Random
import Debug.Trace

data Expr = VarX
		  | VarY
		  | Sin Expr
		  | Cos Expr
		  | Avg Expr Expr
		  | Mult Expr Expr
		  | If Expr Expr Expr Expr

data BuildState = BuildState
	{ rng :: StdGen
	, depth :: Int
	} deriving(Show)

{-buildRandomExpr :: Integral a => a -> StdGen -> Expr-}
buildRandomExpr maxDepth stGen
	| maxDepth < 1 = undefined -- TODO: return a Maybe Expr or ask for a signed type like Word? do something
	| otherwise    = fst $ buildRandomExpr' BuildState{rng = stGen, depth = maxDepth}
		where
			-- refactor buildRandomAvg/Mult to gen a (expr1, expr2) and fmap uncurry Avg/Mult onto it
			buildRandomAvg = do
				expr1 <- state $ buildRandomExpr'
				expr2 <- state $ buildRandomExpr'
				return $ Avg expr1 expr2
			buildRandomMult = do
				expr1 <- state $ buildRandomExpr'
				expr2 <- state $ buildRandomExpr'
				return $ Mult expr1 expr2

			buildRandomIf = do
				exprl <- state $ buildRandomExpr'
				exprr <- state $ buildRandomExpr'
				exprt <- state $ buildRandomExpr'
				exprf <- state $ buildRandomExpr'
				return $ If exprl exprr exprt exprf

			buildRandomExpr' :: BuildState -> (Expr, BuildState)
			buildRandomExpr' buildState = (expr, buildState'''{depth = depth buildState''' + 1})
				where
					{-(num, rng') = randomR (1, 7) $ rng buildState :: (Int, StdGen)-}
					{-buildState' = buildState{rng = rng', depth = depth buildState - 1}-}
					curDepth = depth buildState - 1
					(exprComp, buildState'') =
						if curDepth <= 0
						then
							let
								(num, rng') = randomR (1, 2) $ rng buildState :: (Int, StdGen)
								buildState' = buildState{rng = rng', depth = curDepth}
								randExprComp = case num of
									1 -> return VarX
									2 -> return VarY
							in (randExprComp, buildState')
						else
							let
								(num, rng') = randomR (1, 7) $ rng buildState :: (Int, StdGen)
								buildState' = buildState{rng = rng', depth = depth buildState - 1}
								randExprComp = case num of
									1 -> return VarX
									2 -> return VarY
									3 -> fmap Sin $ state buildRandomExpr'
									4 -> fmap Cos $ state buildRandomExpr'
									5 -> buildRandomAvg
									6 -> buildRandomMult
									7 -> buildRandomIf
							in (randExprComp, buildState')
					(expr, buildState''') = runState exprComp buildState''

buildX = VarX
buildY = VarY
buildSin = Sin
buildCos = Cos
buildAvg = Avg
buildMult = Mult
buildIf = If

instance Show Expr where
	show VarX                         = "x"
	show VarY                         = "y"
	show (Sin expr)                   = "sin(pi * " ++ show expr ++ ")"
	show (Cos expr)                   = "cos(pi * " ++ show expr ++ ")"
	show (Avg expr1 expr2)            = "((" ++ show expr1 ++ " + " ++ show expr2 ++ ") / 2)"
	show (Mult expr1 expr2)           = show expr1 ++ " * " ++ show expr2
	show (If exprl exprr exprt exprf) = show exprl ++ " < " ++
									    show exprr ++ " ? " ++
									    show exprt ++ " : " ++
									    show exprf

-- | > exprToString = show
exprToString :: Expr -> String
exprToString = show

-- | Evaluates a given expression with given x and y.
-- x and y should be [-1, 1].
eval :: (Ord a, Floating a) => Expr -> a -> a -> a
eval VarX x y                              = x
eval VarY x y                              = y
eval (Sin expr) x y                        = sin $ pi * eval expr x y
eval (Cos expr) x y                        = cos $ pi * eval expr x y
eval (Avg expr1 expr2) x y                 = ((eval expr1 x y) + (eval expr2 x y)) / 2
eval (Mult expr1 expr2) x y                = (eval expr1 x y) * (eval expr2 x y)
eval (If exprl exprr exprt exprf) x y
	| (eval exprl x y) < (eval exprr x y)  = eval exprt x y
	| otherwise                            = eval exprf x y
