module Image.PGM (pgm) where

import Data.Int
import Text.Printf
import qualified Data.ByteString.Lazy as B

-- | P5 is the binary version for PGM
data Profile = P5 deriving (Show)
type Gray = Int

def :: PGM
def = PGM P5 255 0 0 []

data PGM = PGM
	{ profile :: Profile
	, columns :: Int
	, rows :: Int
	, maxGray :: Int
	, grays :: [[Gray]]
	}

-- | Turns a 2D array of Gray into a ByteString ready to be written directly.
pgm :: [[Gray]] -> B.ByteString
pgm xs = B.append header body
	where
		spec = from_list xs
		header = asciiBin (printf "%s\n%d %d\n%d\n"
								  (show $ profile spec)
								  (columns spec)
								  (rows spec)
								  (maxGray spec)
								  :: String)
		body = B.concat $ map B.pack $ map (map fromIntegral) $ grays spec

from_list :: [[Gray]] -> PGM
from_list [] = def
from_list xs = def
	{ columns = length $ head xs
	, rows = length xs
	, grays = xs
	}

asciiBin :: String -> B.ByteString
asciiBin = B.pack . map (fromIntegral . fromEnum)
