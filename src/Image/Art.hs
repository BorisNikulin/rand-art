module Image.Art where

import Data.Expr
import Image.PGM

import qualified Data.ByteString.Lazy as B

rescale :: Fractional a => (a, a) -> (a, a) -> a -> a
rescale (min1, max1) (min2, max2) x = ((x - min1) * scaleRatio) + min2
	where scaleRatio = (max2 - min2) / (max1 - min1)

rescaleFrac :: Fractional a => a -> a -> a
rescaleFrac n = rescale (-n, n) (-1 ,1)

rescaleGray :: Fractional a => a -> a
rescaleGray = rescale (-1, 1) (0, 255)

toGrayPixelMap :: (Fractional a, RealFrac b, Enum c, Integral c, Integral d) =>
				  (a -> a -> b) -> c -> [[d]]
toGrayPixelMap f n = map (map $ round) $
					 map (map $ rescaleGray) $
					 map (map $ uncurry f) $
					 map (map . mapPair $ rescaleFrac $ realToFrac n) $
					 map (map . mapPair $ realToFrac) [[(x, y) | x <- [-n..n]] | y <- [-n..n]]
	where mapPair f (x, y) = (f x, f y)
