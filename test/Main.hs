module Main where

import Test.Tasty
import Test.Tasty.SmallCheck

import qualified Data.Expr.Tests

main = defaultMain tests

tests = testGroup "Tests"
	[ Data.Expr.Tests.tests
	]
