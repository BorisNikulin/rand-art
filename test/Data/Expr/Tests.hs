module Data.Expr.Tests
	( tests
	) where

import Test.Tasty
import Test.Tasty.SmallCheck
import Test.Tasty.HUnit

import Data.Expr

tests = testGroup "Data.Expr"
	[ testProperty "VarX" $
		\x y -> (eval VarX  x y :: Double) == x
	, testProperty "VarY" $
		\x y -> (eval VarY  x y :: Double) == y
	, testProperty "Sin" $
		\x y -> (eval (Sin VarX) x y :: Double) == sin (pi * x)
	, testProperty "Cos" $
		\x y -> (eval (Cos VarX) x y :: Double) == cos (pi * x)
	, testProperty "Avg" $
		\x y -> (eval (Avg VarX VarY) x y :: Double) == (x + y) / 2
	, testProperty "Mult" $
		\x y -> (eval (Mult VarX VarY) x y :: Double) == x * y
	, testProperty "If" $
		\x y -> (eval (If VarX VarY VarX VarY) x y :: Double) == if x < y then x else y
	, testCase "Sample Expr 1" $
		eval sampleExpr1 0.5 (-0.5) @?= 0.0
	]

sampleExpr1 = Sin $ Avg VarX VarY

{-testProperty "Coverage Test" $-}
		{-\x y -> eval (buildRandomExpr $ mkStdGen 29) (x :: Double) (y :: Double) >= -1.0-}
	{-, -}
